# d3tm

<!-- badges: start -->
<!-- badges: end -->

The goal of d3tm is to generated a zoomable treemap capable of handling
unbalanced hierachical data.

## Installation

You can install the development version of d3tm from
[GitLab](https://https://gitlab.com/) with:

``` r
devtools::install_git("https://gitlab.com/lajh87/d3tm.git")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(d3tm)
ztm()
```
